all:
	if [ -d "object" ]; then rm -rf object; fi
	mkdir object
	nasm -f elf32 boot/boot.asm -o object/boot.o
	gcc -m32 -Iinclude -c kernel/kernel.c -o object/kernel.o -ffreestanding -O2 -Wall -Wextra 
	ld -m elf_i386 -T link.ld -o reality object/*.o

#ifndef REALITY_KEYBOARD_H
#define REALITY_KEYBOARD_H

#include <util.h>

#include <reality/screen.h>
#include <reality/keyTable.h>


void scanCode(void)
{
  unsigned char i;
  static int lshift, rshift, alt, ctrl;

  do {
    i = inb(0x64);
  } while((i & 0x01) == 0);

  i = inb(0x60);
  i--;

  if(i < 0x80) {
    switch(i) {
      case 0x29:
        lshift = 1;
        break;
      case 0x35:
        rshift = 1;
        break;
      case 0x1C:
        ctrl = 1;
        break;
      case 0x37:
        alt = 1;
        break;
      default:
        putchar(kbdmap[i*4 + (lshift|| rshift)]);
    }
  } else {
    switch(i) {
      case 0x29:
        lshift = 0;
        break;
      case 0x35:
        rshift = 0;
        break;
      case 0x1C:
        ctrl = 0;
        break;
      case 0x37:
        alt = 0;
        break;
    }
  }
}

char getchar(void)
{
  size_t index = termRow * VGA_WIDTH + termCol-1;
  return termBuffer[index];
}

#endif
